use futures_util::{FutureExt, StreamExt, TryStreamExt};
use tokio_postgres::{Error, NoTls};

const SETUP_SQL: &str = r#"
CREATE TABLE IF NOT EXISTS times (
    time     text        NOT NULL
  );
  
  CREATE FUNCTION times_changed()
  RETURNS TRIGGER AS $$
  BEGIN
    EXECUTE 'NOTIFY time_notifications, ''' || NEW.time || '''';
    RETURN NULL; -- result is ignored since this is an AFTER trigger
  END;
  $$ LANGUAGE plpgsql;
  
  CREATE TRIGGER times_changed
   AFTER INSERT OR UPDATE ON times
   FOR EACH ROW EXECUTE FUNCTION times_changed();
   "#;

// start postgres locally:
// `podman run -p 5432:5432 -e POSTGRES_PASSWORD=password postgres`
#[tokio::main]
async fn main() -> Result<(), Error> {
    let (client, connection) =
        tokio_postgres::connect("host=localhost user=postgres password=password", NoTls).await?;
    tokio::spawn(connection);
    client.simple_query(SETUP_SQL).await?;

    tokio::spawn(async {
        listen_for_times().await.unwrap();
    });

    let (client, connection) =
        tokio_postgres::connect("host=localhost user=postgres password=password", NoTls).await?;
    tokio::spawn(connection);
    loop {
        tokio::time::sleep(std::time::Duration::from_secs(5)).await;
        client.simple_query(format!("INSERT INTO times VALUES ('{:?}')", std::time::SystemTime::now()).as_str()).await.unwrap();
    }
}

// Adapted from https://github.com/sfackler/rust-postgres/blob/8b9b5d0388c8961c44b7c107eaa626c24a0b3051/tokio-postgres/tests/test/main.rs#L736-L768
async fn listen_for_times() -> Result<(), Error> {
    let (client, mut connection) =
        tokio_postgres::connect("host=localhost user=postgres password=password", NoTls).await?;
    let (tx, mut rx) = futures_channel::mpsc::unbounded();
    let stream = futures_util::stream::poll_fn(move |cx| connection.poll_message(cx))
        .map_err(|e| panic!("{}", e));
    let connection = stream.forward(tx).map(|r| r.unwrap());
    tokio::spawn(connection);

    client
        .batch_execute("LISTEN time_notifications;")
        .await
        .unwrap();

    loop {
        if let Ok(Some(msg)) = rx.try_next() {
            dbg!(format!("got {msg:?}"));
        }
        tokio::time::sleep(std::time::Duration::from_secs(1)).await;
    }
}
